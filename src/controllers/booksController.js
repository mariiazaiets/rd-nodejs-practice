const express = require('express');
const router = express.Router();

const {
    getBooksByUserId,
    getBookByIdForUser,
    addBookToUser,
    updateBookByIdForUser,
    deleteBookByIdForUser
} = require('../services/booksService');

const {
    asyncWrapper
} = require('../utils/apiUtils');
const {
    InvalidRequestError
} = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    const books = await getBooksByUserId(userId);

    res.json({books});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    const book = await getBookByIdForUser(id, userId);

    if (!book) {
        throw new InvalidRequestError('No book with such id found!');
    }

    res.json({book});
}));

router.post('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    await addBookToUser(userId, req.body);

    res.json({message: "Book created successfully"});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const data = req.body;

    await updateBookByIdForUser(id, userId, data);

    res.json({message: "Book updated successfully"});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    await deleteBookByIdForUser(id, userId);

    res.json({message: "Book deleted successfully"});
}));

module.exports = {
    booksRouter: router
}