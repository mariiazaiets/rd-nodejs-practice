const {Book} = require('../models/bookModel');

const getBooksByUserId = async (userId) => {
    const books = await Book.find({userId});
    return books;
}

const getBookByIdForUser = async (bookId, userId) => {
    const book = await Book.findOne({_id: bookId, userId});
    return book;
}

const addBookToUser = async (userId, bookPayload) => {
    const book = new Book({...bookPayload, userId});
    await book.save();
}

const updateBookByIdForUser = async (bookId, userId, data) => {
    await Book.findOneAndUpdate({_id: bookId, userId}, { $set: data});
}

const deleteBookByIdForUser = async (bookId, userId) => {
    await Book.findOneAndRemove({_id: bookId, userId});
}

module.exports = {
    getBooksByUserId,
    getBookByIdForUser,
    addBookToUser,
    updateBookByIdForUser,
    deleteBookByIdForUser
};